# Using Docker - Asqatasun 4.x  (deprecated)

This is a **deprecated** version of Asqatasun,
we recommend you to use a more recent version: [Asqatasun v5](../5.x).

## Asqatasun 4.1.0

- no docker image for this version of Asqatasun
- you can use Vagrant instead to run Asqatasun 4.1.0 on your computer

see: <https://gitlab.com/asqatasun/asqatasun-vagrant/-/tree/master/4.x/Asqatasun_v4.1.0>

```shell
git clone https://gitlab.com/asqatasun/asqatasun-vagrant.git
cd asqatasun-vagrant/4.x/Asqatasun_v4.1.0
vagrant up
vagrant ssh
    sudo -i  # Inside the box
    cd /vagrant
    ./asqatasun.sh
```

## Asqatasun 4.0.3

see: <https://gitlab.com/asqatasun/Asqatasun/-/tree/v4.0.3/docker>
